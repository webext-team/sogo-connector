sogo-connector (68.0.1-2) unstable; urgency=medium

  * [2bfa6a2] d/control: bump Standards-Version to 4.5.0
  * [6b2381b] d/copyright: small spelling fix
  * No functional changes, pushing a new version so upgrades from buster are
    possible as version in buster was made on a wrong base.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Wed, 05 Feb 2020 12:31:44 +0100

sogo-connector (68.0.1-1) unstable; urgency=medium

  * [0e43d2d] d/control: move Maintainer to Debian Mozilla Extension Maintainers

 -- Carsten Schoenert <c.schoenert@t-online.de>  Tue, 24 Dec 2019 14:18:51 +0100

sogo-connector (68.0.1-1~exp1) unstable; urgency=medium

  * [74a8e5f] New upstream version 68.0.1
  * [5c78ff2] d/control: add new package webext-sogo-connector
    - The source of the package is now web-extension based only, no old
      transitional xul stuff is included. So make this visible by moving the
      main binary package over to webext-* syntax.
  * [b292c29] d/control: remove B-D on make and mozilla-devscripts
    - Drop Build-Depends on make and mozilla-devscripts, they are not needed
      any more.
  * [1dadf9c] d/control: adding Rules-Requires-Root: no
  * [1d3e119] d/rules: updating build targets
    - Clean up all non needed xul-* helpers, makes the mostly needed target
      reduced to the quite the minimum.
  * [ec0863d] webext-sogo-connector adding install sequencer file
  * [366a931] webext-sogo-connector: adding linking sequencer file
  * [1c7252f] webext-sogo-connector: adding docs sequencer file
  * [48e8b6a] d/xul-ext-sogo-connector.lintian-overrides: drop file
    - xul-ext-sogo-connector is now a transitional package, we don't need
      this lintian file any more.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Mon, 23 Dec 2019 13:41:27 +0100

sogo-connector (68.0.0-1) unstable; urgency=medium

  * [63605f6] New upstream version 68.0.0
    (Closes: #945061)
  * [f021239] d/control: bump Standards-Version to 4.4.1
  * [cece803] d/control: drop B-D on python-ply
    (Closes: #939479)
  * [86ab883] rebuild patch queue from patch-queue branch
    - removed patches:
      removing-the-COPYING-file.patch
      sogo-connector.xpt-prepare-option-for-rebuild-the-.x.patch
  * [00917e3] remove now obsolete *.idl files
    - The package build isn't depending on some old files from the non
      existing package thunderbird-dev any more. The build doesn't uses
      *.idl files now.
  * [4b96f9a] d/copyright: update date information
  * [3efbfa0] d/watch: switch over to git mode
  * [324d0b8] d/rules: rewrite targets due modified source for TB 68
    - Rewrite the control of the package build. There is no local run of
      some Make targets needed any more.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 24 Nov 2019 20:41:22 +0100

sogo-connector (60.0.2-1) unstable; urgency=medium

  * [747546e] New upstream version 60.0.2

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 07 Mar 2019 19:40:57 +0100

sogo-connector (60.0.1-1) unstable; urgency=medium

  * [37adbb6] New upstream version 60.0.1
  * [fcd4f5d] d/control: bump Standards-Version to 4.3.0
    - No further changes needed.
  * [0c71fd4] debhelper: use debhelper-compat in B-D
    - Move over to use debhelper-compat (with version 12) instead of using
      a specific debhelper version together with a possible different version
      for compatibility in d/compat.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 21 Feb 2019 19:27:47 +0100

sogo-connector (60.0.0+gite2547a3-1) unstable; urgency=medium

  * [8785a7e] New upstream version 31.0.6
  * [4498ec4] add files from package thunderbird-devel
   - To get the upstream source build we need some files from the now no
     longer available package thunderbird-dev. Extracting these files from
     latest available version on snapshot.d.o and place the files with the
     debian/ folder.
  * [014690f] rebuild patch queue from patch-queue branch
   - modified patch:
     sogo-connector.xpt-prepare-option-for-rebuild-the-.x.patch
     By the now different source folders for the required files from the old
     package thunderbird-dev we also need to modify the Makefile within the
     folder components/ so this file is referencing the new source folder for
     to get the AddOn build.
  * [a787dfb] New upstream version 60.0.0+gite2547a3
   - Closes: #909313, #890513, #858734
  * [e3da533] d/control: remove B-D on thunderbird-dev
   - Removing the no longer available package from the Build-Depends.
  * [99365db] debian/control: bump Standards-Version to 4.2.1
   - No further changes needed.
  * [ca0be35] d/control: adjust Vcs fields to Salsa
   - Packaging tree is now moved over to Salsa.
  * [e4632ef] d/watch: use https instead of http
  * [b31b662] d/control: adjust upstream Homepage
   - Change the referencing Upstream URL to the new created GitHub site on
     https://github.com/inverse-inc/sogo-connector
  * [d02948f] d/watch: use new github sub site for the watch file
   - And also use this URL within the watch file.
  * [cb51d79] rebuild patch queue from patch-queue branch
  * [f380573] d/rules: tweak the installation of some files
   - Remove unneeded Makefile from the package and move the README file into
     /usr/share/doc/xul-ext-sogo-connector

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 28 Sep 2018 19:12:07 +0200

sogo-connector (31.0.5-2) unstable; urgency=medium

  * [d457c90] debian/control: removing references to Icedove
   - Clean out any references to old icedove* packages for Build-Depends and
     also for Depends in the binary package to not collide with removal of
     transitional packages from the src:icedove package.
  * [1afa79a] debian/control: bump Standards-Version to 4.1.0
   - Policy version is now moved on to 4.1.0.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 21 Sep 2017 20:18:57 +0200

sogo-connector (31.0.5-1) unstable; urgency=medium

  * [5ba36c6] rebuild patch queue from patch-queue branch
   - Adding some modifications so the keyword 'let' isn't used for some
     variables in some source files as the variables aren't have a local
     scope. This decreases some unneded error messages on the cli.
  * [a5cefa8] debian/gbp.conf: adjust to branch debian/sid
  * [3256585] New upstream version 31.0.5
  * [21244ff] debian/control: adding X-Debian-Homepage field
   - Adding a extra pointer to the Debian Wiki site on the package tracker
     site.
  * [78b1c01] debian/copyright: small updates
  * [4572780] debian/control: bump Standards-Version to 4.0.0
   - No extra changes needed to archive the new requirements by 4.0.0.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 23 Jul 2017 22:38:28 +0200

sogo-connector (31.0.4-1) unstable; urgency=medium

  * [a6d9434] New upstream version 31.0.4
  * [7e6659e] debian/copyright: update file after upstream changes

 -- Carsten Schoenert <c.schoenert@t-online.de>  Mon, 05 Jun 2017 13:36:53 +0200

sogo-connector (31.0.3-3) unstable; urgency=medium

  * [7f95a11] rebuild patch queue from patch-queue branch (Closes: #856457)
  * [9328a7e] debian/control: reverse the resolving order for -dev package

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 04 Mar 2017 09:51:45 +0100

sogo-connector (31.0.3-2) unstable; urgency=medium

  * [85e9938] debian/control: wrap-and-sort all entries
  * [82988f2] Build-Depends: append possible thunderbird-dev package
  * [6ac973e] debhelper: bump to version 10
  * [1f59a4a] debian/control: correcting, moving Vcs fields to https
  * [c4ff179] debian/copyright: update same sections, adding MPL-1.1
  * [ba1f4ce] debian/control: extend the description of sogo-connector

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 29 Jan 2017 14:40:27 +0100

sogo-connector (31.0.3-1) unstable; urgency=medium

  * debian/control: increase B-D for mozilla-devscripts
  * linitan: bump up standards version to 3.9.8
  * debian/control: adding lightning as additional dependency
  * debian/gbp.conf: additional filter for files while import
  * Imported Upstream version 31.0.3

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 02 Jul 2016 20:13:12 +0200

sogo-connector (31.0.2-1) unstable; urgency=medium

  * Imported Upstream version 31.0.2
  * debian/copyright: small adjustments
  * debian/gbp.conf: respect new git-buildpackage behaviour
  * rebuild patch queue from patch-queue branch

 -- Carsten Schoenert <c.schoenert@t-online.de>  Tue, 12 Jan 2016 20:37:18 +0100

sogo-connector (31.0.1-1) unstable; urgency=medium

  * debian/watch: switching to new scanning URL
  * Imported Upstream version 31.0.1
     fixed upstream bugs: #3042
  * debian/copyright: adopt and fix copyright holder

 -- Carsten Schoenert <c.schoenert@t-online.de>  Tue, 20 Jan 2015 18:47:50 +0100

sogo-connector (31.0.0-1) unstable; urgency=medium

  * debian/control: changing Vcs URL's to Alioth
  * debian/watch: switch to new upstream version 31
  * Imported Upstream version 31.0.0
  * rebuild patch queue from patch-queue branch

 -- Carsten Schoenert <c.schoenert@t-online.de>  Tue, 28 Oct 2014 19:22:22 +0100

sogo-connector (24.0.7-1) unstable; urgency=medium

  * Imported Upstream version 24.0.7

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 10 Oct 2014 08:51:28 +0200

sogo-connector (24.0.6-3) experimental; urgency=medium

  * debian/control: adding dep on iceowl-extension > 31
  * linitan: bump up standards version to 3.9.6
  * debian/control: adding Christoph as uploader

 -- Carsten Schoenert <c.schoenert@t-online.de>  Wed, 08 Oct 2014 20:34:25 +0200

sogo-connector (24.0.6-2) experimental; urgency=medium

  * debian/copyright: adding more files
      Thanks Thorsten Alteholz for pointing!

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 13 Sep 2014 19:35:22 +0200

sogo-connector (24.0.6-1) experimental; urgency=medium

  * Imported Upstream version 24.0.6
  * debian/copyright: reflect recent changes by upstream

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 03 Aug 2014 21:57:21 +0200

sogo-connector (24.0.5-1) experimental; urgency=medium

  [ Guido Günther ]
  * Upstream tarball is bzip2

  [ Carsten Schoenert ]
  * debian/gbp.conf: Upstream tarball is gz
  * Imported Upstream version 24.0.4
  * Revert "debian/control: adjust dependency on iceowl-extension"
  * debian/control: adding iceowl-extension to the recommends
  * debian/control: adding current Vcs fields
  * Imported Upstream version 24.0.5
  * debian/MPL-1.1: adding Mozilla MPL-1.1 license
  * debian/control: removing b-d on maxVersion for icedove-dev
  * debian/copyright: rework the whole copyright file
  * debian/docs: adding some already shipped doc files
  * lintian: adding a override for wrong complained issue

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 03 Jul 2014 21:22:13 +0200

sogo-connector (24.0.2-1) experimental; urgency=low

  * Imported Upstream version 24.0.2
  * debian/control: adjust build-dependency on icedove-dev 24.x
  * debian/control: adjust dependency on iceowl-extension
  * debian/watch: adjust watch file to version 24
  * document changes and release 24.0.2-1

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 30 Jan 2014 21:32:13 +0100

sogo-connector (17.0.6-1) experimental; urgency=low

  * Imported Upstream version 17.0.6
  * debian/control: adding dependencie on icedove-dev and python-ply
  * create possibility for rebuilding 'sogo-connector.xpt'
  * debian/rules: force rebuild of the sogo-connector.xpt file
  * debian/control: add build-dependecy
  * document changes and release 17.0.6-1

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 29 Nov 2013 20:54:46 +0100

sogo-connector (17.0.5-1) experimental; urgency=low

  * Initial release (Closes: #692984)
  * document changes and release 17.0.5-1

 -- Carsten Schoenert <c.schoenert@t-online.de>  Mon, 02 Sep 2013 18:12:29 +0200
